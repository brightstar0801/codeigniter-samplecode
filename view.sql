CREATE VIEW view_keys AS
	SELECT 
		tbl_keys.*,
		tbl_product.name productName,
		tbl_product.contraction,
		tbl_product.creater,
		tbl_payment.type,
		DATE_FORMAT(tbl_keys.regDate, '%Y') AS regYear,
		DATE_FORMAT(tbl_keys.regDate, '%Y%m') AS regMonth
	FROM tbl_keys
	LEFT JOIN tbl_product
		ON tbl_keys.productID = tbl_product.id
	LEFT JOIN tbl_payment
		ON tbl_keys.payType = tbl_payment.id
	
		
CREATE VIEW view_keys_mlog AS
	SELECT 
		tbl_keys_mlog.*,
		tbl_user.name
	FROM tbl_keys_mlog
	LEFT JOIN tbl_user
		ON tbl_keys_mlog.uID = tbl_user.id
		