<?php
/**
 * @name        BASEMODEL
 * @author      Brightstar0801
 * @version     1.0
 * @since       2017-02-01
 */

class Base_model extends CI_Model
{
    /**
     * Function: Data insert function
     * @param string $tblName       
     * @param array  $insertArray   
     */
    function insertData($tblName, $insertArray)
    {
        $this->db->insert($tblName, $insertArray);
        return $this->db->insert_id();
    }
    
    /**
     * Function: Batch Data Array insert function
     * @param string $tblName
     * @param array  $insertBatchArray
     */
    
    function insertBatchData($tblName, $insertBatchArray)
    {
        $this->db->insert_batch($tblName, $insertBatchArray);
    }
    
    /**
     * Function: Data Update function
     * @param string $tblName
     * @param array  $updateArray 
     * @param array or string $where
     */
    function updateData($tblName, $updateArray, $where=array())
    {
        $this->db->update($tblName, $updateArray, $where);
    }
    
    /**
     * Function: Data Delete function
     * @param string $tblName 
     * @param array or string $where
     */
    function deleteData($tblName, $where="")
    {
        $this->db->delete($tblName, $where);
    }
    
    /**
     * Function: Truncate Tabel
     * @param string $tblName
     */
    function truncateTable($tblName)
    {
        $this->db->truncate($tblName);
    }

    /**
     * Function: GET List Data
     * @param string $tblName
     * @param string or array $where 
     * @param number $offset
     * @param number $limit
     * @param string $fields
     * @param array  $orderArray
     */
    function getListData($tblName, $where=array(), $offset=0, $limit=0, $fields="*", $orderArray=array())
    {
        if($fields == "*")
            $this->db->select("*");
        else
            $this->db->select($fields);
    
        if($limit)
            $this->db->limit($limit, $offset);
    
        foreach ($orderArray as $item)
        {
            $this->db->order_by($item['orderField'], $item['orderBy']);
        }
        
        $result = "";
    
        if(is_array($where))
            $result = $this->db->get_where($tblName, $where);
        else 
        {
            $this->db->where($where);
            $result = $this->db->get($tblName);
        }
    
        return $result->result_array();
    }
    
    /**
     * Function: GET ALL LIST DATA
     * @param string $tblName
     * @param string $where 
     * @param number $offset
     * @param number $limit
     * @param string $fields
     * @param array  $orderArray 
     * @return array
     */
    function getAllListData($tblName, $where=array(), $fields="*", $orderArray=array())
    {
       if($fields == "*")
            $this->db->select("*");
        else
            $this->db->select($fields);
    
        foreach ($orderArray as $item)
        {
            $this->db->order_by($item['orderField'], $item['orderBy']);
        }
        
        $result = "";
        
        if(is_array($where))
            $result = $this->db->get_where($tblName, $where);
        else 
        {
            $this->db->where($where);
            $result = $this->db->get($tblName);
        }
    
        return $result->result_array();
    }
    
    /**
     * Function: GET ONE DATA
     * @param string $tblName
     * @param array  $whereArray
     * @param string $fields
     */
    function getOneRowData($tblName, $where=array(), $fields="*")
    {
        if($fields == "*")
            $this->db->select("*");
        else
            $this->db->select($fields);
        
        $result = "";
        
        if(is_array($where))
            $result = $this->db->get_where($tblName, $where);
        else 
        {
            $this->db->where($where);
            $result = $this->db->get($tblName);
        }
        
        return $result->row_array();
    }
    
    /**
     * Function: GET ONE FIELD
     * @param string $tblName
     * @param array  $whereArray
     * @param string $field
     */
    function getOneFieldData($tblName, $whereArray=array(), $field)
    {
        $this->db->select($field);
        $result = $this->db->get_where($tblName, $whereArray);
        
        $row = $result->row_array();
        $fieldData = "";
        
        if($row)
            $fieldData = array_shift($row);
        
        return $fieldData;
    }
    
    /**
     * Function: USING MYSQL QUERY
     * @param string $sql
     */
    function getListDataFromSql($sql)
    {
        $result = $this->db->query($sql);
        return $result->result_array();
    }
    
    /**
     * Function: USING MYSQL QUERY
     * @param string $sql
     */
    function getOneDataFromSql($sql)
    {
        $result = $this->db->query($sql);
        return $result->row_array();
    }
    
    /**
     * Function: USING MYSQL QUERY
     * @param string $sql
     */
    function getOneFieldFromSql($sql)
    {
        $result = $this->db->query($sql);
        
        $row = $result->row_array();
        $fieldData = "";
        
        if($row)
            $fieldData = array_shift($row);
        
        return $fieldData;
    }
    
    /**
     * Function: GET ROW COUNT
     * @param string $tblName
     * @param string $whereArray
     * @return number
     */
    function getRowCount($tblName, $where=array())
    {
        $result = "";
        
        if(is_array($where))
            $result = $this->db->get_where($tblName, $where);
        else
        {
            $this->db->where($where);
            $result = $this->db->get($tblName);
        }
        
        $count  = $result->result_id->num_rows;
        return $count;
    }
    
    /**
     * Function: GET LAST QUERY
     */
    function getLastQuery()
    {
        return $this->db->last_query();
    }

	function runSql($sql)
	{
		$result = $this->db->query($sql);
	}
    
    function echo_array($array)
    {
        echo "<pre>";
        print_r($array);
        echo "</pre>";
    }
}