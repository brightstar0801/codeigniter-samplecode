<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Function: comeback function for stirng of parent module
 * @param array $moduleSortArray
 * @param number $selID
 * @param string $spec
 * @param number $level
 * @return string moduleOptStr
 */
if ( ! function_exists('_getModuleOptStr'))
{
    function _getModuleOptStr($moduleArray, $selPTcode, $spec, $oldTCode)
    {
        $moduleOptStr = "";
         
        foreach ($moduleArray as $item)
        {
            $subTCode = substr($item['treeCode'], 0, strlen($oldTCode));

            if(($oldTCode == $item['id'] || $oldTCode == $subTCode) && $oldTCode) continue;

            $selected = ($selPTcode == $item['treeCode'])?"selected":"";
            
            $tCodelength = strlen($item['treeCode']);
            
            $space = str_repeat($spec, (strlen($item['treeCode']) - 2));
            $moduleOptStr .= "<option value='".$item['treeCode']."' ".$selected.">".$space.$item['moduleName']."</option>";
        }

        return $moduleOptStr;
    }
}

/**
 * Function: 
 */
if ( ! function_exists('_makeModuleClassArray'))
{
    function _makeModuleClassArray($moduleArray, $permission=array())
    {
        $moduleClassArray = array();
        
        foreach ($moduleArray as $key => $moduleItem)
        {
            $tCodeLength = strlen($moduleItem['treeCode']);
            
            if($tCodeLength == 2)
            {
                $moduleClassArray[$moduleItem['treeCode']] = $moduleItem;
            }
            else 
            {
                $pTreeCode = substr($moduleItem['treeCode'], 0, 2);
                $parent = & $moduleClassArray[$pTreeCode];
                
                for ($i = 2; $i < $tCodeLength ; $i += 2)
                {
                    $subTCode = substr($moduleItem['treeCode'], 0, $i);
                    
                    if (isset($parent['subModules'][$subTCode])) 
                    {
                        $parent = & $parent['subModules'][$subTCode];
                    }
                }
                
                $parent['subModules'][$moduleItem['treeCode']] = $moduleItem;
            }
        }
        
        return $moduleClassArray;
    }
}

/**
 * Function: 
 */
if ( ! function_exists('_makeModuleParentArray'))
{
    function _makeModuleParentArray($moduleArray, $permission=array())
    {
        $moduleListForID = array();  

        foreach ($moduleArray as $item)
        {
            $moduleListForID[$item['id']] = $item;
        }
        
        foreach ($moduleListForID as $key => $item)  
        {
            $parent = & $moduleListForID[$item['parentID']];
            $parent['children'][$key] = & $moduleListForID[$key];
        }
         
        $moduleParentArray = $moduleListForID[0]['children'];
         
        return $moduleParentArray;
    }
}


/**
 * Function: modulelist
 * @param array $moduleSortArray
 * @param string $spec
 * @param number $level
 * @param string $path
 */
$moduleListArray = array();
$loopCount = 0;

if ( ! function_exists('_getModuleListArrayForListView'))
{
    function _getModuleListArrayForListView($moduleSortArray, $spec="", $level=0, $path, $actionArray)
    {
        global $moduleListArray, $loopCount;
        
        foreach ($moduleSortArray as $key => $item)
        {
            $space = str_repeat( $spec, $level*2 );
    
            $actionStr    = $item['actions'];
            $actionConStr = "";
    
            if ($item['moduleType'] == 'C')
            {
                $actionArray1 = explode(";", $actionStr);
    
                foreach ($actionArray1 as $actItem)
                {
                    $actionConStr .= (isset($actionArray[$actItem])?$actionArray[$actItem]:$actItem) . ", ";
                }
    
                $actionConStr = rtrim($actionConStr, ", ");
            }
    
            $totalPath = $path . "/" . $item['baseName'];
    
            $moduleListArray[$loopCount] = array(
                'id'            => $item['id'],
                'moduleName'    => $space . $item['moduleName'],
                'moduleType'    => $item['moduleType'],
                'baseName'      => $item['baseName'],
                'path'          => $totalPath,
                'parentID'      => $item['parentID'],
                'actions'       => $actionConStr,
                'sort'          => $item['sort'],
                'fontIcon'      => $item['fontIcon'],
                'isEnable'      => $item['isEnable'],
                'isShow'        => $item['isShow'],
            );
    
            $loopCount += 1;
    
            if(isset($item['children']))
            {
                _getModuleListArrayForListView($item['children'], "..", $level+1, $totalPath, $actionArray);
            }
        }
    }
}
?>