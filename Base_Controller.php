<?php 
/**
 * @author      Bright Code Studio
 * @copyright   BCStudio
 * @version     3.2
 * @desc        base controller
 * @since       2017-10-01    
 */

class Base_Controller extends CI_Controller 
{
    public $controllerUrl = "";
    public $backUrl       = "";
    public $permssion     = array();
    
    /**
     * constructor
     * @desc load the constructor
     */
    function __construct()
    {
        parent::__construct();  

        $this->load->model('Base_model', 'baseM');
        $this->load->model('Split_model', 'splitM');
        
        $this->load->helper('system');
        $this->load->helper('variable');
        $this->load->helper('cookie');
        
        $this->load->library('user_agent');
        
        $this->_accessLog();
        $this->_isLoginCheck();
        
		$this->_updateVisitTime();

        $this->permssion = json_decode($this->session->permission, true);
    }
    
    /**
     * Function: login check
     * @param $array
     */
    protected function _isLoginCheck()
    {
        $isLoginFlag = $this->session->userdata('isLoginFlag');

		$accessIP	 = $this->input->ip_address();
		$accessAgent = $this->agent->agent_string();

		if ($accessIP == '172.16.4.40' && $accessAgent == "PostmanRuntime/2.5.0")
			$isLoginFlag = true;
    
        if (!isset($isLoginFlag) || $isLoginFlag != true)
        {
            redirect('welcome/');
        }
    }
	
	/**
	 * Function: show page
	 */
	protected function _showPage($data)
	{
	    $segmentsArray = $this->uri->segments;
	    $segCount = count($segmentsArray);
	    
	    if($segCount < 3) exit;
	    
	    $headData['topNavMenuArray'] = $this->_createTopNavMenuArray();

	    $headData['colorArray']      = _getAceColorClass();
	    $headData['selNavMenu']      = $segmentsArray['1'];
	    
	    $sideData['stepBarArray']    = $this->_getStepbarArray();
	    $sideData['sideMenuArray']   = $this->_createSidebarMenuArray($segmentsArray['1']);
	    $sideData['selSideMenu']     = $segmentsArray['2'];
	    $sideData['selSideSubMenu']  = $segmentsArray['3'];
	    
	    $checkFlag = $this->_checkPermission();

	    if($checkFlag)
	    {
	        $folderPath = $segmentsArray['1'] . "/" . $segmentsArray['2'] . "/" . $segmentsArray['3'];
	        $fileName   = isset($segmentsArray['4'])?$segmentsArray['4']."_view":"index_view";
	        
			$isMangeOpinion = $this->_checkManageByMID(91);

			if($isMangeOpinion)
			{
				$headData['isMangeOpinion'] = $isMangeOpinion;

				$whereArray['isRead'] = 0;
				$orderArray            = array(array('orderField'=>'insertDate','orderBy'=>'DESC'));

				$headData['opinionList'] = $this->baseM->getAllListData(TBL_OPINION_LIST, $whereArray, "*", $orderArray);
				$tempArray = array();
				foreach ($headData['opinionList'] as $key => $item) {
					
					$wherePhoto['name'] = $item['userName'];
					$tempArray[$item['userName']] = $this->baseM->getOneRowData(VIEW_EMP_PROFILE, $wherePhoto, "photo");
				}
				$headData['photoList'] = $tempArray;
			}
			
			//$this->echo_array($headData['isMangeOpinion']);exit;
	        $this->load->view('page_layout/header', $headData);
	        $this->load->view('page_layout/sidebar', $sideData);
	        $this->load->view($folderPath.'/'.$fileName, $data);
	         
	        $this->load->view('page_layout/footer');
	    }
	    else 
	    {
			$this->_unLawLog();

            $this->load->view('page_layout/header', $headData);
	        $this->load->view('page_layout/sidebar', $sideData);
	        $this->load->view('errors/html/error_perm');
	        $this->load->view('page_layout/footer');        
	    }
	}
	
	protected function _showTemplatePage($data=array())
	{
	    $segmentsArray = $this->uri->segments;
	    
	    $headData['topNavMenuArray'] = $this->_createTopNavMenuArray();
	    $headData['colorArray']      = _getAceColorClass();
	    $headData['selNavMenu']      = $segmentsArray['2'];
	    
	    $sideData['sideMenuArray']   = $this->_createSidebarMenuArray($segmentsArray['2']);
	    $sideData['stepBarArray']    = $this->_getStepbarArray();
	    $sideData['selSideMenu']     = "";
	    $sideData['selSideSubMenu']  = "";
	    
	    $this->load->view('page_layout/header', $headData);
	    $this->load->view('page_layout/sidebar', $sideData);
	    $this->load->view('template_view', $data);
	    $this->load->view('page_layout/footer.php');
	}
	
	/**
	 * Function: permission check function
	 */
	protected function _checkPermission()
	{
	    $rsegmentsArray = $this->uri->rsegments;
	    $modName = $rsegmentsArray['1'];
	    
	    $modID = $this->baseM->getOneFieldData(TBL_SYS_MODULES, array("moduleType" => "C", "baseName" => $modName), "id");
	    
	    $checkFlag = false;
	    foreach ($this->permssion as $key => $item)
	    {
	        if($key == $modID) 
	        {
	            $checkFlag = true;
	            break;
	        }
	    }
	    
	    return $checkFlag;
	}

	
	protected function _checkProjID($projID)
	{
	    $projIDArray = $this->session->userdata('projIDArray');
	   
		if (in_array($projID, $projIDArray) || !$projID)
 			return true;
		else
			return false;
	}
	
	protected function _getRatioFromProjID($projID)
	{
		$allData = $this->baseM->getAllListData(TBL_PROJ_BASE_PLAN,array('projID' => $projID),"projID,state,weight");
		
		$allWeight = 0;
		foreach($allData as $item)
		{
			$allWeight += $item['weight'];
		}

		$finishData = $this->baseM->getAllListData(TBL_PROJ_BASE_PLAN,array('projID' => $projID,'state' => 3),"projID,state,weight");
		
		$finishWeight = 0;
		foreach($finishData as $item)
		{
			$finishWeight += $item['weight'];
		}
		
		return round(($finishWeight/$allWeight)*100);
	}
	
	
	/**
	 * Function: SideMenu
	 */
	protected function _createSidebarMenuArray($navTopMenu)
	{
	    $pTCode = $this->baseM->getOneFieldData(TBL_SYS_MODULES, array('baseName' => $navTopMenu), "treeCode pTCode");

	    $whereArray['isEnable'] = 1;
	    $whereArray['treeCode LIKE '] = $pTCode."%";
	    $whereArray['LENGTH(treeCode) > '] = 2;
	    
	    $fields     = "id, fontIcon, moduleName, baseName, treeCode, actions";
	    $orderArray = array(array('orderField' => 'treeCode', 'orderBy' => 'ASC'));
	     
	    $tempModuleArray = $this->_getModuleArray($whereArray, $fields, $orderArray);
	    $sideMenuArray   = _makeModuleClassArray($tempModuleArray);
	    
	    if(isset($sideMenuArray[$pTCode]['subModules']))
	        return $sideMenuArray[$pTCode]['subModules'];
	    else 
	        return $sideMenuArray;
	    
	}
	
	private function _getStepbarArray()
	{
	    $segmentsArray = $this->uri->segments;
	    
	    $stepBarArray = array();
	    
	    foreach ($segmentsArray as $item)
	    {
	        $whereArray = array('baseName' => $item, 'moduleType' => 'F');
	        $fields     = "moduleName, fontIcon";
	        
	        $stepBarData = $this->baseM->getOneRowData(TBL_SYS_MODULES, $whereArray, $fields);
	        
	        if($stepBarData) $stepBarArray[] = $stepBarData;
	    }
	    
	    return $stepBarArray;
	}
	
	public function _getModuleArray($whereArray=array(), $fields="*", $orderArray=array())
	{
	    $moduleArray = $this->baseM->getAllListData(TBL_SYS_MODULES, $whereArray, $fields, $orderArray);
	    $permModArray = array();
 	     
	    foreach ($moduleArray as $key => $item)
	    {
	        $moduleArray[$key]['moduleUrl'] = $this->_getModuleURL($item['treeCode']);
          
	        $tempPermArray = $this->permssion; 
	        if(isset($tempPermArray[$item['id']]))
	        {
	            $permModArray[] = $moduleArray[$key];
	        }
	    }

	    return $permModArray;
	}
	
	public function _getModuleURL($treeCode)
	{
	    $moduleUrl  = "";
	    $finalTCode = $this->_getFinalTCode($treeCode);
	    
	    if($finalTCode)
	    {
	        $tCodeLength = strlen($finalTCode);
	        
	        for($i = 2; $i <= $tCodeLength; $i = $i+2)
	        {
	            $subTreeCode = substr($finalTCode, 0, $i);
	            $baseName = $this->baseM->getOneFieldData(TBL_SYS_MODULES, array('treeCode' => $subTreeCode), "baseName");
	            $moduleUrl .= $baseName ."/";
	        }
	    }
	    else
	    {
	        $tCodeLength = strlen($treeCode);
	         
	        for($i = 2; $i <= $tCodeLength; $i = $i+2)
	        {
	            $subTreeCode = substr($treeCode, 0, $i);
	            $baseName = $this->baseM->getOneFieldData(TBL_SYS_MODULES, array('treeCode' => $subTreeCode), "baseName");
	            $moduleUrl .= $baseName ."/";
	        }
	        
	        $moduleUrl = 'template/'.$moduleUrl;
	    }
	    
	    return $moduleUrl;
	}
	
	public function _getFinalTCode($tCode)
	{
	    $where  = "moduleType = 'C' AND isEnable = 1";
	    $where .= " AND treeCode LIKE '". $tCode."%'";
	    
	    $modIDArray = $this->baseM->getAllListData(TBL_SYS_MODULES, $where, "id");

	    $tempPermArray = array_keys($this->permssion);
	    
	    $permMIDStr = "";
	    foreach ($modIDArray as $item)
	    {
	        if (in_array($item['id'], $tempPermArray)) $permMIDStr .= $item['id']. ",";
	    }

	    $permMIDStr = rtrim($permMIDStr, ",");
	    
	    if($permMIDStr) $where .= " AND id IN (". $permMIDStr. ")";
	    
	    $field = "MIN(treeCode) finalTCode";
	    
	    $finalTCode = $this->baseM->getOneFieldData(TBL_SYS_MODULES, $where, $field);	    
	    return $finalTCode;
	}
	
	/**
	 * Get Controller Path function 
	 */
	public function _getControllerPath()
	{
	    $segmentsArray  = $this->uri->segments;   // codeigniter URL Array
	    $rsegmentsArray = $this->uri->rsegments;  // codeigniter Controller Array
	     
	    $controllerPath = "";
	    foreach ($segmentsArray as $segment)
	    {
	        $controllerPath .= $segment ."/";
	        if($segment == $rsegmentsArray['1']) break;
	    }
	     
	    return $controllerPath;
	}

	/**
	 * Last visit time Update function
	 */
	private function _updateVisitTime()
	{
		$empID = $this->session->userdata('empID');
		$this->baseM->updateData(TBL_EMP_ACCOUNT, array('visitTime' => time()), array('empID' => $empID));
	}
	
	/**
	 * accesslog function 
	 */
	private function _accessLog()
	{
	    $insertArray['accessIP']   = $this->input->ip_address();
	    $insertArray['accessDate'] = date("Y-m-d H:i:s");
	    
	    $postDJson = json_encode($this->input->post());
	    $getDJson  = json_encode($this->input->get());
	    
	    $accessUrl  = "URL: ".uri_string();
	    $accessUrl .= ($postDJson != "[]")?"<br>POST: ".$postDJson:"";
	    $accessUrl .= ($getDJson != "[]")?"<br>GET: ".$getDJson:"";
	    
	    $insertArray['accessUrl']  = $accessUrl;
	    $insertArray['agent']      = $this->_getAgent("custom");
	    
	    $this->splitM->insertDataToSplitTbl(TBL_LOG_ACCESS, $insertArray, "QUARTER");
	}

	/**
	 * unlaw access check function
	 */
	protected function _unLawLog($unlawText="FAKEACCESS")
	{
	    $insertArray['accessIP']   = $this->input->ip_address();
	    $insertArray['accessDate'] = date("Y-m-d H:i:s");
	    
	    $postDJson = json_encode($this->input->post());
	    $getDJson  = json_encode($this->input->get());
	    
	    $accessUrl  = "URL: ".uri_string();
	    $accessUrl .= ($postDJson != "[]")?"<br>POST: ".$postDJson:"";
	    $accessUrl .= ($getDJson != "[]")?"<br>GET: ".$getDJson:"";
	    
	    $insertArray['accessUrl']  = $accessUrl;
	    $insertArray['agent']      = $this->_getAgent("custom");

		$insertArray['unlawText']  = $unlawText;
		$insertArray['userName']   = $this->session->userdata('name');
	    
	    $this->splitM->insertDataToSplitTbl(TBL_LOG_UNLAW, $insertArray, "YEAR");
	}

	/**
	 * access Agent function 
	 */
	private function _getAgent($type="default")
	{
	    $agent = "";
	     
	    if($type == "default")
	    {
	        $agent = $this->agent->agent_string();
	    }
	    else
	    {
	        $agent = $this->agent->platform();
	         
	        if ($this->agent->is_browser())
	        {
	            $agent .= " => " . $this->agent->browser().' '.$this->agent->version();
	        }
	        elseif ($this->agent->is_robot())
	        {
	            $agent .= " => " . $this->agent->robot();
	        }
	        elseif ($this->agent->is_mobile())
	        {
	            $agent .= " => " . $this->agent->mobile();
	        }
	        else
	        {
// 	            $agent .= " => " . 'Unidentified User Agent';
	            $agent = $this->agent->agent_string();
	        }
	    }
	     
	    return $agent;
	}
}
