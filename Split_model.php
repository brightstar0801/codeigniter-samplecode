<?php
/**
 * @name        Table Auto-Split Model
 * @author      Brightstar
 * @version     2.0
 * @since       2017-02-01
 * @uses        
 * @desc        table split quarter or year.
 */

class Split_model extends CI_Model
{
    /**
     * @var database object
     */
    var $historyDB;
    
    var $historyForge;
    
    /**
     * @var history database name
     */
    var $hisDBName;
    
    
    /**
     * construct
     */
    function __construct()
    {
        parent::__construct();
        
        $this->load->dbutil();
        $this->load->dbforge();
        
        $this->hisDBName = $this->_createHistoryDatabase();
        $this->historyDB = $this->_connectHistoryDatabase($this->hisDBName);
        $this->historyForge = $this->load->dbforge($this->historyDB, TRUE);
    }
    
    /**
     * Function: Insert the data to split Table
     * @param string $tblName       org table name
     * @param array  $insertArray   insert data
     * @param string $type          YEAR, QUARTER
     * @param number $months
     */
    public function insertDataToSplitTbl($orgTblName, $insertArray, $type="YEAR")
    {
        $suffix = $this->_getSuffixForSplitTable($type);
        
        $newTblName = $orgTblName . "_" . $suffix;
        
        if (! $this->_isExistTableName($newTblName))
        {
            $this->_createSplitTable($orgTblName, $newTblName);
        }
        
        $this->historyDB->insert($newTblName, $insertArray);
        return $this->historyDB->insert_id();
    }
    
    /**
     * Function: Delete the data from splited table
     * @param string $splTblName  splited table name
     * @param string $where
     */
    function deleteDataForSplit($splTblName, $where="")
    {
        $this->historyDB->delete($splTblName, $where);
    }
    
    /**
     * Function: Truncate data from splited table
     * @param string    $orgTblName
     */
    function truncateSplitTable($orgTblName)
    {
        $splitTblNameArray = $this->_getSplitTableNameArray($orgTblName);
        
        foreach ($splitTblNameArray as $sTblName)
        {
            $this->historyDB->truncate($sTblName);
        }
    }
    
    /**
     * Function: Drop splited tables 
     * @param string    $tblName
     */
    function dropSplitTable($tblName)
    {
        $splitTblNameArray = $this->_getSplitTableNameArray($tblName);
        
        foreach ($splitTblNameArray as $sTblName)
        {
            $this->historyForge->drop_table($sTblName['Tables_in_'.$this->hisDBName], TRUE);
        }
    }
    
    /**
     * Function: Get Row Count from Splited Table
     * @param string            $splTblName splited table
     * @param string || array   $where
     * @return unknown
     */
    public function getRowCountForSplitTbl($splTblName, $where)
    {
        $result = "";
        
        if(is_array($where))
            $result = $this->historyDB->get_where($splTblName, $where);
        else
        {
            $this->historyDB->where($where);
            $result = $this->historyDB->get($splTblName);
        }
        
        $count  = $result->result_id->num_rows;
        return $count;
    }
    
    /**
     * Function: Get data from splited table 
     * @param string            $splTblName splited table name
     * @param string || array   $where
     * @param number            $offset
     * @param number            $limit
     * @param string            $fields
     * @param array             $orderArray
     */
    function getListDataForSplitTbl($splTblName, $where=array(), $offset=0, $limit=0, $fields="*", $orderArray=array())
    {
        if($fields == "*")
            $this->historyDB->select("*");
        else
            $this->historyDB->select($fields);
    
        if($limit)
            $this->historyDB->limit($limit, $offset);
    
        foreach ($orderArray as $item)
        {
            $this->historyDB->order_by($item['orderField'], $item['orderBy']);
        }
        
        $result = "";
    
        if(is_array($where))
            $result = $this->historyDB->get_where($splTblName, $where);
        else 
        {
            $this->historyDB->where($where);
            $result = $this->historyDB->get($splTblName);
        }
    
        return $result->result_array();
    }
    
    /**
     * Function: Get Table name
     * @param string $orgTblName
     * @param string $year          2017
     * @param string $month         q1-0, q1-01
     * @return string|boolean
     */
    public function getSplitQuarterTblName($orgTblName, $year, $month)
    {
        $newTblName = $orgTblName . "_" . $year;
        $monthArray = explode("-", $month);
        
        if(isset($monthArray[0]))
        {
            $newTblName .= "_" . $monthArray[0];
        }
        
        $existTblFlag = $this->_isExistTableName($newTblName);
        
        if($existTblFlag)
            return $newTblName;
        else
            return false;
    }
    
    /**
     * Function: Check the table
     * @param string $tblName
     * @return boolean
     */
    private function _isExistTableName($tblName)
    {
        $sql  = "SHOW TABLES ";
        $sql .= " WHERE tables_in_" . $this->hisDBName . " = '" .$tblName . "'";
        
        $result = $this->historyDB->query($sql);
        $count  = $result->result_id->num_rows;
        
        if($count)
            return true;
        else 
            return false;
    }
    
    /**
     * Function: Get the name array of Splited tables
     * @param string $orgTblName
     */
    private function _getSplitTableNameArray($orgTblName)
    {
        $sql  = "SHOW TABLES ";
        $sql .= " WHERE tables_in_" . $this->hisDBName . " LIKE '" .$orgTblName . "%'";
        
        $result = $this->historyDB->query($sql);
        
        return $result->result_array();
    }
    
    /**
     * Function: Get suffix of split table
     * @param string $type
     * @return string
     */
    private function _getSuffixForSplitTable($type="YEAR")
    {
        $suffix = "";
        
        switch ($type)
        {
            case "QUARTER":  // Quearter
                $year  = date("Y");
                $month = date("n");
                
                $sufMonth = ((int)($month / 3));
                
                if($month % 3) $sufMonth += 1;
                
                $suffix   = $year."_q".$sufMonth;
                break;
                
            case "YEAR":  // Year
                $suffix = date("Y");
                break;    
        }
        
        return $suffix;
    }
    
    /**
     * Function: Create the Splited table
     * @param string $orgTblName
     * @param string $newTblName
     */
    private function _createSplitTable($orgTblName, $newTblName)
    {
        $query = $this->db->query("SHOW CREATE TABLE `".$orgTblName.'`');
        
        $result = $query->row_array();
        
        $createTblQuery = $result['Create Table'];
        $createTblQuery = str_replace("CREATE TABLE `$orgTblName`", "CREATE TABLE IF NOT EXISTS `$newTblName`", $createTblQuery);
        
        $this->historyDB->query($createTblQuery);
    }
    
    /**
     * Function: Create the History Database and Return table lists of Database
     * @return string
     */
    private function _createHistoryDatabase()
    {
        $historyDbName = $this->db->database . "_history";
        $historyDbName = strtolower($historyDbName);

        if (!$this->dbutil->database_exists($historyDbName))
        {
           $this->dbforge->create_database($historyDbName);
        }
        
        return $historyDbName;
    }
    
    /**
     * Function: Create the DB_Driver Object to access history database
     * @param string $historyDbName
     * @return CI_DB_driver object
     */
    private function _connectHistoryDatabase($historyDbName)
    {
        global $db;
        
        $hisDBConfigArray = $db['default'];
        $hisDBConfigArray['database'] = $historyDbName;
        
        $dbDriverObj = $this->load->database($hisDBConfigArray, TRUE);
        
        return $dbDriverObj;
    }
    
    public function getLastQuery()
    {
        return $this->historyDB->last_query();
    }
    
    private function echo_array($array)
    {
        echo "<pre>";
        print_r($array);
        echo "<pre>";
    }
}